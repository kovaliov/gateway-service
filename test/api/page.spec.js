'use strict';

const apiInspect = require('api-inspect');

describe('API', () => {
  let api = null;

  beforeEach(function() {
    api = apiInspect.getInstance();
    api.setApi('http://localhost:3000/api/v2');
  });

  it('/pages should send invalid assert structure without Accept-Language', () => {
    api.get('/pages')
      .header({
        'language': 'de_DE',
        'accept': 'application/json',
        'platform': 'ott',
      })
      .test(inspect => {
        // console.log(inspect.res.req);
        inspect.statusCode(406);
      });
  });

  it('/pages should send valid assert structure with Accept-Language', () => {
    api.get('/pages')
      .header({
        'Accept-Language': 'de_DE',
        'accept': 'application/json',
        'language': 'de_DE',
        'platform': 'ott',
      })
      .test(inspect => {
        inspect.statusCode(200);
      });
  });

  it('/pages should send invalid assert structure without language', () => {
    api.get('/pages')
      .header({
        'accept': 'application/json',
        'Accept-Language': 'de_DE',
        'platform': 'ott',
      })
      .test(inspect => {
        inspect.statusCode(406);
      });
  });

  it('/pages should send valid assert structure with language', () => {
    api.get('/pages')
      .header({
        'Accept-Language': 'de_DE',
        'language': 'de_DE',
        'accept': 'application/json',
        'platform': 'ott',
      })
      .test(inspect => {
        inspect.statusCode(200);
      });
  });

  it('/pages should send valid assert structure platform ott', () => {
    api.get('/pages')
      .header({
        'Accept-Language': 'de_DE',
        'accept': 'application/json',
        'language': 'de_DE',
        'platform': 'ott',
      })
      .test(apiInspect => {
        apiInspect.statusCode(200);
      });
  });

  it('/pages should send valid assert structure platform web', () => {
    api.get('/pages')
      .header({
        'Accept-Language': 'de_DE',
        'accept': 'application/json',
        'language': 'de_DE',
        'platform': 'web',
      })
      .test(apiInspect => {
        apiInspect.statusCode(200);
      });
  });

  it('/pages should send valid assert structure platform mobile', () => {
    api.get('/pages')
      .header({
        'Accept-Language': 'de_DE',
        'accept': 'application/json',
        'language': 'de_DE',
        'platform': 'mobile',
      })
      .test(apiInspect => {
        apiInspect.statusCode(200);
      });
  });

  it('/pages should send invalid assert structure platform tv', () => {
    api.get('/pages')
      .header({
        'Accept-Language': 'de_DE',
        'accept': 'application/json',
        'language': 'de_DE',
        'platform': 'tv',
      })
      .test(apiInspect => {
        apiInspect.statusCode(406);
      });
  });

  it('/pages should send invalid assert structure without Accept', () => {
    api.get('/pages')
      .header({
        'Accept': '',
        'Accept-Language': 'de_DE',
        'accept': 'application/json',
        'platform': 'tv',
      })
      .test(apiInspect => {
        apiInspect.statusCode(406);
      });
  });

  afterEach(function() {
    return api.done;
  });
});