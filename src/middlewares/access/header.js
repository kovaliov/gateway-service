'use strict';

module.exports.inject = function(LoggerFactory) {
  const logger = LoggerFactory.getCategoryLogger('Header-Middleware');

  return function(req, res, next) {
    if (req.method === 'OPTIONS') {
      logger.info(200, 'no header checks on preflight calls');

      next();

      return;
    }

    if (!req.get('Accept')) {
      logger.info(406, 'Accept header is missing');

      res.status(406).json({
        message: 'Accept header is missing'
      });

      return;
    }

    const platform = req.get('platform');

    if (platform === 'web' || platform === 'ott' || platform === 'mobile') {
      logger.info(200, 'Platform header is OK');
    } else {
      logger.info(406, 'Platform header is missing for request.');

      res.status(406).json({
        message: 'Accept header is missing'
      });

      return;
    }

    const acceptLanguage = req.get('Accept-Language');

    if (acceptLanguage === 'de_DE') {
      logger.info(200, 'Accept-Language header is OK');
    } else {
      logger.info(406, 'Accept-Language header is missing or not accepted for this route');

      res.status(406).json({
        message: 'Accept-Language header is missing or not accepted for this route'
      });

      return;
    }

    const language = req.get('language');

    if (language === 'de_DE') {
      logger.info(200, 'language header is OK');
    } else {
      logger.info(406, 'language header is missing or not accepted for this route');

      res.status(406).json({
        message: 'language header is missing or not accepted for this route'
      });

      return;
    }

    next();
  };
};
