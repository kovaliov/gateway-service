'use strict';

module.exports.inject = function (LoggerFactory) {
  const apiRouter = require('express').Router();

  const pageRouter = require('./v2/page').inject(LoggerFactory);

  apiRouter.use('/pages', pageRouter);

  return apiRouter;
};
