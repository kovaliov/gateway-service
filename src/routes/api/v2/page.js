'use strict';

module.exports.inject = function(LoggerFactory) {
  const pageRouter = require('express').Router();
  const requestLogger = LoggerFactory.getCategoryLogger('REQUEST');

  pageRouter.get('/', (req, res, next) => {
    requestLogger.info(req.originalUrl);

    res.status(200).json({
      accept: req.header('Accept'),
      acceptLanguage: req.header('Accept-Language'),
      language: req.header('language'),
      platform: req.header('platform')
    });
  });

  return pageRouter;
};
