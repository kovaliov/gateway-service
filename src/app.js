'use strict';

const directory = `${__dirname}/../config`;
const config = require('mxd-yaml-config-lib')([
  directory + '/all.yml',
  directory + '/development.yml' // TODO: return back "directory + '/' + (process.env.NODE_ENV || 'development') + '.yml'"
]);

const co = require('co');

const environment = require('mxd-environment')(config);
const LoggerFactory = { getCategoryLogger: environment.logging };

const apiLogger = LoggerFactory.getCategoryLogger('API');

process.on('uncaughtException', function(err) {
  apiLogger.error('uncaughtException', err);

  process.exit(1);
});

const startupSequence = function* () {
  apiLogger.info(`Initiating startup sequence in environment ${process.env.NODE_ENV}`);

  const PORT = process.env.PORT || config.port;

  const ExpressUtil = require('./utils/express-util').inject(config, LoggerFactory); // TODO: arg -> LoggerFactory, redisClients, cacheClient
  const app = ExpressUtil.makeApp();

  yield ExpressUtil.startWebServerOnPort(app, PORT);
};

co(startupSequence)
  .then(() => apiLogger.info('All systems set up. Service is good to go'))
  .catch((err) => {
    apiLogger.error('Fatal error during startup. Server is Shutting down', err);

    process.exit(1);
  });
