'use strict';

const bodyParser = require('body-parser');
const cors = require('mxd-cors');
const superagent = require('superagent');
const expressMung = require('express-mung');
const express = require('express');
const co = require('co');
const lodash = require('lodash');

module.exports.inject = function(config, LoggerFactory) { // TODO: arg -> redisClients, cacheClient
  const API_VERSION = config.api.version;

  const apiLogger = LoggerFactory.getCategoryLogger('API');

  return class ExpressUtil {
    static startWebServerOnPort(_app, port) {
      return new Promise((resolve, reject) => {
        apiLogger.info(`Attempting to start API ${API_VERSION} on port ${port}`);

        _app.listen(port, function() {
          const SERVER_ADDRESS = this.address().address;
          const SERVER_PORT = this.address().port;

          apiLogger.info(`API ${API_VERSION} listening at http://${SERVER_ADDRESS}:${SERVER_PORT}`);

          resolve(this.address);
        });
      });
    }

    static makeApp() {
      const app = express();

      const CORS_CONFIG = {
        allowedHeaders: ['content-type', 'accept',
          'clienttype', 'x-maxdome-capability',
          'platform', 'client',
          'language', 'mxd-session',
          'cache-control', 'customerid',
          'maxdome-origin', 'x-device', 'deviceid']
      };

      const apiRouter = require('../routes/api/api.js').inject(LoggerFactory);

      app.use(cors(CORS_CONFIG));

      app.use(bodyParser.json());
      app.use(bodyParser.raw({ type: 'application/xml' }));

      app.use(require('../middlewares/access/header').inject(LoggerFactory));

      app.use('/api/v2', apiRouter);

      return app;
    }
  };
};
